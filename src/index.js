import suma from './utils/sum';

const hello = 'hello';

console.log({ hello });
console.log(suma(2, 5));

/**
 * Archivo de entrada para el webpack.
 * Antes de poder ejecutar se necesita ejecutar los siguientes comandos:
 * 
 * npm init -y                     [el parametro -y deja una configuración por defecto]
 * npm install webpack webpack-cli [Instalamos la librería webpack]
 * npx webpack --mode production   [Compila y genera la salida de la aplicación en un carpeta dist/.. en modo produción
 *                                  lo que quiere decir es que optimizará el código y demás configuraciones que le brindemos.]
 * npx webpack --mode development  [Compila y genera la salida de la aplicación en un carpeta dist/.. en modo desarrollo
 *                                  lo que quiere decir es que mostrará la compilación completa en el archivo de salida]
 */
